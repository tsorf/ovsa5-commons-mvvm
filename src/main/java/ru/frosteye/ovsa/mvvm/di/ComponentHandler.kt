package ru.frosteye.ovsa.mvvm.di

import android.app.Application
import dagger.android.DispatchingAndroidInjector

class ComponentHandler constructor(
    private val application: Application,
    private val injector: DispatchingAndroidInjector<Any>
) {


    fun registerForActivityType(marker: Class<*>) {
        if (activeMarkersSet.contains(marker)) return
        activeMarkersSet.add(marker)
        application.registerActivityLifecycleCallbacks(InjectorActivityCallbacks(injector, marker))
    }

    companion object {

        private val activeMarkersSet = mutableSetOf<Class<*>>()
    }
}
