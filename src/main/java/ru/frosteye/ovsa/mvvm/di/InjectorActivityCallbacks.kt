package ru.frosteye.ovsa.mvvm.di

import android.app.Activity
import android.app.Application
import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import dagger.android.AndroidInjector
import ru.frosteye.ovsa.mvvm.view.BasicView
import ru.frosteye.ovsa.commons.rx.disposable.DisposableTrashCan

internal class InjectorActivityCallbacks(
    private val injector: AndroidInjector<Any>,
    private val marker: Class<*>
) : Application.ActivityLifecycleCallbacks {

    private val attachedActivities = mutableListOf<Activity>()

    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
        filter(activity) {
            injector.inject(activity)
        }
        if (activity is FragmentActivity) {
            activity.supportFragmentManager
                .registerFragmentLifecycleCallbacks(InjectorFragmentCallbacks(injector, marker), true)
        }
    }

    override fun onActivityStarted(activity: Activity) {
        filter(activity) {
            if (activity is BasicView<*> && !attachedActivities.contains(activity)) {
//                activity.attachPresenter()
                attachedActivities.add(activity)
            }
        }
    }

    override fun onActivityResumed(activity: Activity) {

    }

    override fun onActivityPaused(activity: Activity) {

    }

    override fun onActivityStopped(activity: Activity) {

    }

    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {

    }

    override fun onActivityDestroyed(activity: Activity) {
        filter(activity) {
            if (activity is DisposableTrashCan) {
                activity.disposeTrashCan()
            }
            /*if (activity is LiveView) {

            }*/
            attachedActivities.remove(activity)
        }
    }

    private fun filter(activity: Activity, action: () -> Unit) {
        if (marker.isInstance(activity)) {
            action()
        }
    }

}