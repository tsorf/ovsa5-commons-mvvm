package ru.frosteye.ovsa.mvvm.di

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import dagger.android.AndroidInjector
import ru.frosteye.ovsa.commons.rx.disposable.DisposableTrashCan

internal class InjectorFragmentCallbacks(
    private val injector: AndroidInjector<Any>,
    private val marker: Class<*>
) : FragmentManager.FragmentLifecycleCallbacks() {

    override fun onFragmentCreated(
        fm: FragmentManager,
        fragment: Fragment,
        savedInstanceState: Bundle?
    ) {
        filter(fragment) {
            injector.inject(fragment)
        }
    }

    override fun onFragmentViewCreated(
        fm: FragmentManager,
        fragment: Fragment,
        v: View,
        savedInstanceState: Bundle?
    ) {

        filter(fragment) {
            /*if (fragment is Mvp.View<*>) {
                fragment.attachPresenter()
            }*/
        }
    }

    override fun onFragmentDestroyed(fm: FragmentManager, fragment: Fragment) {
        filter(fragment) {
            /*if (fragment is Mvp.View<*>) {
                fragment.presenter.onDestroy()
            }*/
            if (fragment is DisposableTrashCan) {
                fragment.disposeTrashCan()
            }
        }
    }

    private fun filter(fragment: Fragment, action: () -> Unit) {
        if (marker.isInstance(fragment)) {
            action()
        }
    }
}