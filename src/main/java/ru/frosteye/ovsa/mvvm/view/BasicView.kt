package ru.frosteye.ovsa.mvvm.view

import androidx.lifecycle.LifecycleOwner
import ru.frosteye.ovsa.commons.extensions.observe
import ru.frosteye.ovsa.mvvm.router.MvvmRouter
import ru.frosteye.ovsa.commons.messages.AppMessage
import ru.frosteye.ovsa.commons.messages.Messenger

interface MessengerView {

    val messenger: Messenger?

    fun afterMessage(message: AppMessage, accepted: Boolean) {

    }
}

interface ControlsView {

    fun enableControls(enabled: Boolean, controlsGroup: ControlsGroup = ControlsGroup.All) {

    }
}

abstract class ControlsGroup {

    object All : ControlsGroup()
}

interface RouterView<R : MvvmRouter> {

    val router: R
}


interface BasicView<R : MvvmRouter> : LiveView, MessengerView, ControlsView, RouterView<R> {

    fun observeMessenger(
        owner: LifecycleOwner,
        listener: (AppMessage, Boolean) -> Unit = this::afterMessage
    ) {
        val messages = viewModel?.messages ?: return
        owner.observe(messages) { message ->
            messenger?.message(message)?.subscribe({
                listener(message, it)
            }, {
                it.printStackTrace()
            })
        }
    }

    fun observeControlsState(
        owner: LifecycleOwner,
        listener: (Boolean, ControlsGroup) -> Unit = this::enableControls
    ) {
        val controlsState = viewModel?.controlsState ?: return
        owner.observe(controlsState) { message ->
            listener(message.enabled, message.controlsGroup)
        }
    }


}

