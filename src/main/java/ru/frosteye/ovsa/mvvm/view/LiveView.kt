package ru.frosteye.ovsa.mvvm.view

import ru.frosteye.ovsa.mvvm.viewmodel.LiveViewModel

interface LiveView {

    val viewModel: LiveViewModel?
}