package ru.frosteye.ovsa.mvvm.router

interface RouterResult {

    val type: Type
    val requestCode: String

    enum class Type {
        SUCCESS,
        CANCELED,
        FAILURE
    }
}