package ru.frosteye.ovsa.mvvm.router

interface MvvmRouter {

    fun navigateBack(result: RouterResult? = null)
}