package ru.frosteye.ovsa.mvvm.viewmodel

import androidx.lifecycle.LiveData
import ru.frosteye.ovsa.mvvm.model.ControlsState
import ru.frosteye.ovsa.commons.messages.AppMessage

interface LiveViewModel {

    val controlsState: LiveData<ControlsState>
    val messages: LiveData<AppMessage>
}