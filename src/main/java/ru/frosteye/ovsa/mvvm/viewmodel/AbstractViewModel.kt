package ru.frosteye.ovsa.mvvm.viewmodel

import androidx.annotation.CallSuper
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.rxjava3.disposables.CompositeDisposable
import ru.frosteye.ovsa.mvvm.model.ControlsState
import ru.frosteye.ovsa.mvvm.view.ControlsGroup
import ru.frosteye.ovsa.commons.messages.AppMessage
import ru.frosteye.ovsa.commons.rx.disposable.DisposableTrashCan

open class AbstractViewModel : ViewModel(), DisposableTrashCan, LiveViewModel {

    override val disposableContainer: CompositeDisposable = CompositeDisposable()

    private val _controlsState: MutableLiveData<ControlsState> = MutableLiveData()
    override val controlsState: LiveData<ControlsState> = _controlsState

    private val _messages: MutableLiveData<AppMessage> = MutableLiveData()
    override val messages: LiveData<AppMessage> = _messages

    @CallSuper
    override fun onCleared() {
        disposableContainer.dispose()
    }

    protected fun postControlsState(enabled: Boolean, controlsGroup: ControlsGroup = ControlsGroup.All) {
        _controlsState.value = ControlsState(enabled, controlsGroup)
    }

    protected fun postMessage(
        message: CharSequence?,
        title: CharSequence? = null,
        action: CharSequence? = null,
        code: Int = 0
    ) {
        _messages.value = AppMessage.info(message, title, action, code)
    }

    protected fun postSuccess(
        message: CharSequence?,
        title: CharSequence? = null,
        action: CharSequence? = null,
        code: Int = 0,
        payload: Any? = null
    ) {
        _messages.value = AppMessage.success(message, title, action, code, payload)
    }

    protected fun postError(
        message: CharSequence?,
        title: CharSequence? = null,
        action: CharSequence? = null,
        code: Int = 0,
        error: Throwable? = null
    ) {
        _messages.value = AppMessage.error(message, title, action, code, error)
    }
}