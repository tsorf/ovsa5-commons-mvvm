package ru.frosteye.ovsa.mvvm.model

import ru.frosteye.ovsa.mvvm.view.ControlsGroup

data class ControlsState(
    val enabled: Boolean,
    val controlsGroup: ControlsGroup
)